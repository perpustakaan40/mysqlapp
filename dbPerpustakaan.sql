CREATE DATABASE `perpustakaan` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

CREATE TABLE `customers` (
  `userid` int NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `namadepan` varchar(45) NOT NULL,
  `namabelakang` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `borrows` (
  `borrowid` int NOT NULL,
  `borrowdate` datetime DEFAULT NULL,
  `userid` int NOT NULL,
  `bookid` varchar(45) NOT NULL,
  `bookname` varchar(45) DEFAULT NULL,
  `isactive` tinyint NOT NULL,
  PRIMARY KEY (`borrowid`),
  KEY `REFERENCES_idx` (`userid`),
  CONSTRAINT `REFERENCES` FOREIGN KEY (`userid`) REFERENCES `customers` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
